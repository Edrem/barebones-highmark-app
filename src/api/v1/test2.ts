import { Router } from "express";

/**
 * More robust response, allowing you to use middleware, specify GET, POST etc
 */
const router = Router();
router.get("/", (req, res) => {
    res.json({ test: 2 });
});

export default router;
