import { Request, Response } from "express";

/**
 * Simple API response with just a function
 */
export default (req: Request, res: Response) => {
    res.json({ test: 1 });
}