import * as React from "react";
import style from "./index.scss";
import PushDataTag from "perpl/PushDataTag";

export default function () {
    return (
        <div className={style.container}>
            <PushDataTag>
                {() => (
                    <>
                        <title>Barebones Perpl App</title>
                        <meta name="description" content="A simple website built with perpl.js" />
                    </>
                )}
            </PushDataTag>
            <div>
                It works!
            </div>
        </div>
    )
}